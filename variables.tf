# The environment name

variable "environment" {
    type = string
    description = "The name of our environment. Defaults to demo"
    default = "demo"
}

variable "owner" {
    type = string
    description = "The name of the owner that created resources, defaults to devops"
    default = "devops"
}

variable "location" {
    type = string
    description = "The region location where to create resources, defaults to eastus"
    default = "westeurope"
}

variable "app" {
    type = string
    description = "A nickname of the second vm"
    default = "appVM"
}

variable "bastion" {
    type = string
    description = "A nickname for the first vm"
    default = "bastionVM"
}

variable "username" {
    type = string
    description = "The name of our user. Defaults to ttrupina"
    default = "ttrupina"
}

variable "bastionkey" {
    type = string
    description = "public key of the bastion vm"
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQClaRtH/IaXDvL1yU9ODpKcCbLZOVByy108vB+OgLte4jq76VovtVBulw0TSdaH95NkzPcqGjBjUSEdIlqCvEITIer+F1G/epSU0pcr1AVkOrCVq+JQKMcL/6TYdULgb7wybpoAB7GYpv3+lZs+XjhW+MNuANYoP2Xj5J/Hb7FhdX0Mmoyn9H9/zDlHs5r3NmesxHQCx1ufA3tWr4UEH92GVusHjhypcn35IVC9c+Ym0eVWWIuDZ9kCNiV5pZUBI9mQgUv+8Rq6LVqyhHoiGB2HCE5osfJV3kYLMLoxo1EqPybCR3ox1R+RkhycIf5YngOGdbIzNo8jiOc4yE6Ql/Kt"
}

variable "appkey" {
    type = string
    description = "public key of the app vm"
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDUHr2pVqK0RdrAd7WfiUy7C3TdyrI+X3kwqFzo+RQ5f3cf74KjXhnThFVjeLj5yBPInzbVovKbi5SBGY28DrmagLp8X9n0NBO2qjLBdVQvWVOwZTNt33aByBwj6jpTa4QnpSU4ZTSmMLvQeQnQ3F1sqrqA9Myz8yz9C4bYybeO4pEcl9Hwg/o8K/+X1rEwkwHscZX1wQYOhJUEhXTXUBO0UIQ/ScCt1ZBVa5hJICo8dO/7JHodYHU2S+2DTP/hLMDh9U7PavLZBI0Vple31qZOSHDbD36qzyu0mfyQOUsqiUtJupfICzOMrvUyDWHID4Vl6jDg8SWo/rjRIBeTD/FR"
}




