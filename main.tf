# Configure the Microsoft Azure Provider
provider "azurerm" {
    version = "~>2.0"
    features {}
}

# Configure state to be saved in storage account
terraform {
  backend "azurerm" {
    resource_group_name   = "demo-storage"
    storage_account_name  = "demoterraform2021"
    container_name        = "terraform"
    key                   = "terraform.tfstate"
  }
}

# Create a resource group for all of our resources
resource "azurerm_resource_group" "resource_group" {
    name                = "terraform-resource-group-${var.environment}"
    location            = var.location
    tags = {
        environment = var.environment
        owner       = var.owner
    }
}

# Create virtual network in that resource group
resource "azurerm_virtual_network" "vnet" {
    name                = "terraform-vnet-${var.environment}"
    address_space       = ["10.0.0.0/16"]
    location            = var.location
    resource_group_name = "${azurerm_resource_group.resource_group.name}"
    tags = {
        environment = var.environment
        owner       = var.owner
    }
}

# Create public subnet in the VNET from our resource group for bastion/public VMs
resource "azurerm_subnet" "public_subnet" {
    name                 = "${var.bastion}-subnet-${var.environment}"
    resource_group_name  = "${azurerm_resource_group.resource_group.name}"
    virtual_network_name = "${azurerm_virtual_network.vnet.name}"
    address_prefix       = "10.0.0.0/24"
    #network_security_group_id = "${azurerm_network_security_group.public_nsg.id}"
    
    # List of Service endpoints to associate with the subnet.
    service_endpoints         = [
    "Microsoft.ServiceBus",
    "Microsoft.ContainerRegistry"
  ]
}


# Create Network Security Group and ssh rule for public subnet
resource "azurerm_network_security_group" "public_nsg" {
    name                = "${var.environment}-public-nsg"
    location            = var.location
    resource_group_name = "${azurerm_resource_group.resource_group.name}"

    # Allow SSH traffic in from Internet to public subnet.
    security_rule {
        name                       = "allow-ssh-all"
        priority                   = 102
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = var.environment
        owner       = var.owner
    }
}

# Associate network security group with public subnet.
resource "azurerm_subnet_network_security_group_association" "public_subnet_assoc" {
  subnet_id                 = "${azurerm_subnet.public_subnet.id}"
  network_security_group_id = "${azurerm_network_security_group.public_nsg.id}"
}

# Create a public IP address for bastion host VM in public subnet.
resource "azurerm_public_ip" "public_ip" {
    name                         = "public-ip-${var.environment}"
    location                     = "${var.location}"
    resource_group_name          = "${azurerm_resource_group.resource_group.name}"
    allocation_method            = "Dynamic"
    tags = {
        environment = var.environment
        owner       = var.owner
    }
}

# Create network interface for bastion host VM in public subnet.
resource "azurerm_network_interface" "bastion_nic" {
    name                      = "bastion-nic-${var.environment}"
    location                  = "${var.location}"
    resource_group_name       = "${azurerm_resource_group.resource_group.name}"
    #network_security_group_id = "${azurerm_network_security_group.public_nsg.id}"
    
    ip_configuration {
        name                          = "${var.bastion}-nic-cfg-${var.environment}"
        subnet_id                     = "${azurerm_subnet.public_subnet.id}"
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = "${azurerm_public_ip.public_ip.id}"
    }
    tags = {
        environment = var.environment
        owner       = var.owner
    }
}

# Create private subnet for hosting app VMs.
resource "azurerm_subnet" "private_subnet" {
  name                      = "${var.app}-subnet-${var.environment}"
  resource_group_name       = "${azurerm_resource_group.resource_group.name}"
  virtual_network_name      = "${azurerm_virtual_network.vnet.name}"
  address_prefix            = "10.0.1.0/24"
  #network_security_group_id = "${azurerm_network_security_group.private_nsg.id}"

  # List of Service endpoints to associate with the subnet.
  service_endpoints         = [
    "Microsoft.Sql",
    "Microsoft.ServiceBus"
  ]
}

# Create network security group and SSH rule for private subnet.
resource "azurerm_network_security_group" "private_nsg" {
  name                = "private-nsg-${var.environment}"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.resource_group.name}"

  #Allow SSH traffic in from public subnet to private subnet.
  security_rule {
    name                       = "allow-ssh-public-subnet"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "10.0.0.0/24"
    destination_address_prefix = "*"
  }

  #Allow HTTP traffic on port 80 for the appVM
  security_rule {
    name                       = "allow-http-public-subnet_2"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  
  tags = {
        environment = var.environment
        owner       = var.owner
    }
}

# Associate network security group with private subnet.
resource "azurerm_subnet_network_security_group_association" "private_subnet_assoc" {
  subnet_id                 = "${azurerm_subnet.private_subnet.id}"
  network_security_group_id = "${azurerm_network_security_group.private_nsg.id}"
}

# Create a public IP address for app host VM
resource "azurerm_public_ip" "public_ip_app" {
    name                         = "public-ip_app-${var.environment}"
    location                     = "${var.location}"
    resource_group_name          = "${azurerm_resource_group.resource_group.name}"
    allocation_method            = "Dynamic"
    tags = {
        environment = var.environment
        owner       = var.owner
    }
}

# Create network interface for app host VM in private subnet.
resource "azurerm_network_interface" "app_nic" {
    name                      = "app-nic-${var.environment}"
    location                  = "${var.location}"
    resource_group_name       = "${azurerm_resource_group.resource_group.name}"
    #network_security_group_id = "${azurerm_network_security_group.private_nsg.id}"

    ip_configuration {
        name                          = "${var.app}-nic-cfg-${var.environment}"
        subnet_id                     = "${azurerm_subnet.private_subnet.id}"
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = "${azurerm_public_ip.public_ip_app.id}"
    }
    tags = {
        environment = var.environment
        owner       = var.owner
    }
}

resource "azurerm_virtual_machine" "bastion_vm" {
    name                  = "${var.bastion}"
    location              = "${var.location}"
    resource_group_name   = "${azurerm_resource_group.resource_group.name}"
    network_interface_ids = ["${azurerm_network_interface.bastion_nic.id}"]
    vm_size               = "Standard_F2"
    
    storage_os_disk {
        name              = "bastion-dsk001-${var.environment}"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "bastion-vm001-${var.environment}"
        admin_username = "${var.username}"
    }

    os_profile_linux_config {
        disable_password_authentication = true

        # Bastion host VM public key.
        ssh_keys {
            path       = "/home/${var.username}/.ssh/authorized_keys"
            key_data   = "${var.bastionkey}"
        }
    }

    tags = {
        environment = var.environment
        owner       = var.owner
    }
}


#Create app host VM.
resource "azurerm_virtual_machine" "app_vm" {
    name                  = "${var.app}"
    location              = "${var.location}"
    resource_group_name   = "${azurerm_resource_group.resource_group.name}"
    network_interface_ids = ["${azurerm_network_interface.app_nic.id}"]
    vm_size               = "Standard_F2"
    storage_os_disk {
        name              = "${var.environment}-wrkr-dsk001"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Standard_LRS"
    }
    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }
    
    os_profile {
        computer_name  = "wrkr-vm001-${var.environment}"
        admin_username = "${var.username}"
        custom_data    =  "${file("${path.module}/script.sh")}"
    }

    os_profile_linux_config {
        disable_password_authentication = true

        # app host VM public key.
        ssh_keys {
            path     = "/home/${var.username}/.ssh/authorized_keys"
            key_data = "${var.appkey}"
        }
    }
    
    tags = {
        environment = var.environment
        owner       = var.owner
    }
}


resource "azurerm_managed_disk" "disk" {
  name                 = "disk1"
  location             = azurerm_resource_group.resource_group.location
  resource_group_name  = azurerm_resource_group.resource_group.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 10
}

resource "azurerm_virtual_machine_data_disk_attachment" "disk_attach" {
  managed_disk_id    = azurerm_managed_disk.disk.id
  virtual_machine_id = azurerm_virtual_machine.app_vm.id
  lun                = "10"
  caching            = "ReadWrite"
}
   
